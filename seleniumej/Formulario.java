package seleniumej;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import auxClasses.Browsers;
import auxClasses.Loggin;
import auxClasses.ReadUrlFile;



public class Formulario extends Browsers{
	
	@BeforeClass 
	public void setUp(){
		Loggin l = new Loggin();
		l.log(); 
	}
	
	@Test
	public void crearNuevaSolicitudDeLOGO(){
		//nos ubicamos en la pagina correcta luego de iniciar sesion
		driver.get("http://compras135.ufm.edu/adm_academico_select_facultad.php?action=adm_academico_admin_facultades.php&changeTitle=true");
		ReadUrlFile.waitForLoad(driver);
		ReadUrlFile.Wait(1000);
		//luego que carga la pagina accedemos al administrador de logos
		driver.findElement(By.id("tdItemMenu_4")).click();
		driver.findElement(By.xpath(".//*[@id='externo_4_16']/tbody/tr/td/a")).click();
		ReadUrlFile.waitForLoad(driver);
		//guardamos la referencia a la ventana original
		String parentWin = driver.getWindowHandle();
		//Pedimos el formulario para una nueva solicitud de logo
		driver.findElement(By.name("buttClassListadoLogosNuevaSolicitud")).click();
		ReadUrlFile.waitForLoad(driver);
		ReadUrlFile.Wait(1000);
		//cambiamos a la nueva ventana
		for(String childWin : driver.getWindowHandles()){
		    driver.switchTo().window(childWin);
		}
		//llenamos el formulario
		//nombre
		driver.findElement(By.id("txtNombre")).sendKeys("Karl Heinz Ch�vez Asturias");		
		//carne
		driver.findElement(By.id("txtCarne")).sendKeys("20100057");
		//categoria
		Select categlist = new Select(driver.findElement(By.id("sltLogoOrigen")));
		categlist.selectByIndex(1);
		//nuevo logo
		driver.findElement(By.xpath(".//*[@id='tdSltLogoActividad']/button")).click();
		driver.findElement(By.xpath(".//*[@id='tdSltLogoActividad']/div[1]/ul/li[2]/label/span")).click();
		//titulo
		driver.findElement(By.id("txtNombreActividad")).sendKeys("TestXIK_Gonzalez");
		//descripcion
		driver.findElement(By.id("txtDescripcion")).sendKeys("Ingrese una descripci�n.");
		//cantidad de logos
		driver.findElement(By.name("txtLogoAcredita")).sendKeys("9");
		//reposicion
		driver.findElement(By.name("txtLogoReposicion")).sendKeys("6");
		//ciclo
		driver.findElement(By.xpath(".//*[@id='divEditSubCiclo']/button")).click();
		driver.findElement(By.xpath(".//*[@id='divEditSubCiclo']/div/ul/li[3]/label/span")).click();
		//cupo
		driver.findElement(By.name("txtCupo_1_1")).sendKeys("27");
		//seccion
		driver.findElement(By.name("txtSeccion_1_1")).sendKeys("A");		
		//dejamos un momento para corroborar la informacion
		ReadUrlFile.Wait(3000);
		//enviamos la solicitud
		driver.findElement(By.id("buttSendSolicitud")).click();
		//cerramos la ventana actual
		driver.close();		
		//regresamos a la ventana original
		ReadUrlFile.Wait(1000);
		driver.switchTo().window(parentWin);
		//hacemos la busqueda para verificar que la solicitud fue creada
		driver.findElement(By.id("txtListadoLogo_Titulo")).sendKeys("TestXIK_Gonzalez");
		driver.findElement(By.id("buttClassListadoLogosBuscar")).click();
		ReadUrlFile.Wait(5000);
	}	
}
