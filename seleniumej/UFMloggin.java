package seleniumej;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;

import auxClasses.Browsers;
import auxClasses.ReadUrlFile;

public class UFMloggin extends Browsers{
	
	@Test
	public void logInUFMCompras135() {		
		try {
			driver.get("http://compras135.ufm.edu/");
			driver.findElement(By.name("btnLoginGoogle")).click();
			driver.findElement(By.name("identifier")).sendKeys("xik@ufm.edu");
			driver.findElement(By.xpath(".//*[@id='identifierNext']/div[2]")).click();
			ReadUrlFile.Wait(1000);
			driver.findElement(By.name("password")).sendKeys("xik$2015");
			driver.findElement(By.xpath(".//*[@id='passwordNext']/div[2]")).click();
			ReadUrlFile.Wait(4000);
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
	}
	
}
