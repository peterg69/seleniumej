package seleniumej;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import auxClasses.Browsers;
import auxClasses.Loggin;
import auxClasses.ReadUrlFile;

public class Emulacion extends Browsers{
	
	@BeforeClass 
	public void setUp(){
		Loggin l = new Loggin();
		l.log(); 
	}
	
	@Test
	public void emularCatedraticoSubirArchivoAUnCurso() throws AWTException{
		WebDriverWait wait = new WebDriverWait(driver, 10);
		//nos dirigimos a MiU luego de iniciar sesion
		driver.get("http://compras135.ufm.edu/MiU/index.php");
		driver.findElement(By.id("miu_")).click();
		ReadUrlFile.waitForLoad(driver);
		//buscamos las herramientas y accedemos a soporte al estudiante
		driver.findElement(By.id("divFecla_7")).click();
		driver.findElement(By.xpath(".//*[@id='divContentdivFecla_7']/table/tbody/tr[5]/td[2]")).click();
		ReadUrlFile.waitForLoad(driver);
		//mandamos el carne a emular
		driver.findElement(By.id("txtSeleccionUsuario")).sendKeys("20080633");
		//esperamos que la opcion se habilite seleccionamos y emulamos
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='ui-id-4']")));
		driver.findElement(By.xpath(".//*[@id='ui-id-4']")).click();
		driver.findElement(By.id("bttSearch")).click();
		ReadUrlFile.waitForLoad(driver);
		//nos dirigimos a cursos actuales
		driver.findElement(By.id("divFecla_3")).click();
		driver.findElement(By.xpath(".//*[@id='divContentdivFecla_3']/table/tbody/tr[2]/td[2]")).click();
		ReadUrlFile.waitForLoad(driver);
		//guardamos la referencia a la ventana original
		String parentWin = driver.getWindowHandle();
		//seleccionamos el curso
		driver.findElement(By.xpath(".//*[@id='contentTabCursosActuales']/div[5]/table/tbody/tr[3]/td[2]/span")).click();
		ReadUrlFile.waitForLoad(driver);
		//cambiamos a la nueva ventana
		for(String childWin : driver.getWindowHandles()){
			driver.switchTo().window(childWin);
		}
		//elegimos la opcion para agregar archivos
		driver.findElement(By.xpath(".//*[@id='contentTabInformacion']/div[2]/div/div[5]/div/table[2]/tbody/tr[1]/td[2]/span[2]")).click();
		//esperamos que la ventana de dialogo cargue
		wait.until(ExpectedConditions.elementToBeClickable(By.name("txtTituloDocumento_1")));
		//llenamos algunos de los campos
		driver.findElement(By.name("txtTituloDocumento_1")).sendKeys("TEST");
		driver.findElement(By.id("txtDescripcionDocumento_1")).sendKeys("Esto es una prueba para verificar si se pueden subir archivos");
		//abrimos el explorador de archivos
		driver.findElement(By.xpath(".//*[@id='tdflDocumentos_1_1']/span[1]")).click();
		//colocamos el path del archivo en el clipboard
	    StringSelection ss = new StringSelection("C:\\Users\\Xik #1\\Pictures\\wut.png");
	    Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null);
	    //creamos un robot para imitar eventos como ENTER, CTRL+C, CTRL+V
	    Robot robot = new Robot();
	    robot.keyPress(KeyEvent.VK_ENTER);
	    robot.keyRelease(KeyEvent.VK_ENTER);
	    robot.keyPress(KeyEvent.VK_CONTROL);
	    robot.keyPress(KeyEvent.VK_V);
	    robot.keyRelease(KeyEvent.VK_V);
	    robot.keyRelease(KeyEvent.VK_CONTROL);
	    robot.delay(500);
	    robot.keyPress(KeyEvent.VK_ENTER);
	    robot.delay(50);
	    robot.keyRelease(KeyEvent.VK_ENTER);	
		//subimos el archivo 
		driver.findElement(By.id("bttAceptarDocumentos")).click();
		ReadUrlFile.waitForLoad(driver);			
		//Dejamos un tiempo para corroborar que el archivo fue agregado de manera correcta
		ReadUrlFile.Wait(3000);
		//cerramos la ventana actual
		driver.close();				
		//regresamos a la ventana original
		driver.switchTo().window(parentWin);
		//terminamos la emulacion de catedratico
		driver.findElement(By.xpath("html/body/header/div/div[2]/div[3]/table/tbody/tr[3]/td/div/input")).click();
		ReadUrlFile.waitForLoad(driver);
		ReadUrlFile.Wait(1000);
	}
	
}
